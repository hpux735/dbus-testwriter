use dbus::arg::{RefArg, PropMap};
use dbus::Message;

fn write_hash(args: &PropMap, serial: &mut u32) -> String {
    let mut local_serial = *serial;
    let local_hash = format!("hash_{}", local_serial);
    let mut retval = format!("    let mut {}: PropMap = HashMap::new();\n", local_hash);

    for (key, arg) in args.iter() {
        let map: Option<&PropMap> = dbus::arg::cast(&arg.0);

        // Try an integer variant
        if let Some(value) = arg.as_i64() {
            retval += &format!("    {}.insert(\"{}\".to_owned(), Variant(Box::new({})));\n", local_hash, key, value);
        }

        // Try a string variant
        else if let Some(value) = arg.as_str() {
            retval += &format!("    {}.insert(\"{}\".to_owned(), Variant(Box::new(r#\"{}\"#.to_owned())));\n", local_hash, key, value);
        }

        // Try a propmap
        else if let Some(map) = map {
            local_serial += 1;
            let inner_hash = format!("hash_{}", local_serial);
            retval += &write_hash(map, &mut local_serial);
            retval += &format!("    {}.insert(\"{}\".to_owned(), Variant(Box::new({})));\n", local_hash, key, inner_hash);
        }

        else {
            println!("Unable to cast {}: {:?} {:?}", key, arg, map);
        }
    }

    // Increment the provided serial number
    *serial = local_serial;

    retval
}

pub fn write_test(message: &Message) -> String {
    if let Some(member) = message.member() {
        let p = &member;

        let mut retval = format!("fn {}_letter_from_args() -> Message {{\n", p);

        let opt_args: Result<PropMap, _> = message.read1();
        if let Ok(args) = opt_args {
            let mut serial = 0;
            retval += &write_hash(&args, &mut serial);
        }
        
        retval += "\n";
        retval +=          "    Message::new_signal(\n";
        retval += &format!("        \"{}\",\n", &message.interface().unwrap());
        retval += &format!("        \"{}\",\n", &message.path().unwrap());
        retval += &format!("        \"{}\",\n", &message.member().unwrap());
        retval +=          "    ).unwrap().append1(hash_0)\n";

        retval += "}";

        retval
    } else {
        "".to_owned()
    }
}