# DBus TestWriter

This is a very simple little library that just prints out a function that tries to replicate a received dbus message.

This can be useful when you want to write a unit test against some DBus signal.

If this doesn't work with some signal you're seeing, please print out the message with `println!("{:?}", message)` and let me know so I might be able to fix it.

This is my first Rust crate, so be kind ;)